import {Component, OnInit} from '@angular/core';
import {any} from 'codelyzer/util/function';
import {UpdatesService} from '../services/updates.service';

@Component({
  selector: 'app-filtra-dados',
  templateUrl: './filtra-dados.component.html',
  styleUrls: ['./filtra-dados.component.css']
})
export class FiltraDadosComponent implements OnInit{
  updates: any[];
  constructor(private service: UpdatesService) {
     this.updates = [];
  }

  valorCampo = '';
  nomeCampo = '';

  ngOnInit(): void {
       this.service.listar().subscribe((updates: any[]) => {
          this.updates = updates;
       });
    }

  consultar(): void {
    this.service.consultar(this.nomeCampo, this.valorCampo ).subscribe((updates: any[]) => {
      this.updates = updates;
    });
  }

}
